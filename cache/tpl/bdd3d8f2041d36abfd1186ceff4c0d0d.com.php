<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-WDM-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a href="#" class="navbar-brand">Z-CARTEL</a>
		</div>
		<div class="collapse navbar-collapse navbar-right" id="bs-WDM-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="?c=Main&m=profile">Profile</a></li>
				<li><a href="?c=Feed&m=index">Dashboard</a></li>
				<li><a href="?c=Main&m=logout">Signout</a></li>
          </ul>
		</div>
	</div>
</nav>
<div class="wrapper">
	<div class="container">
		<div class="col-md-12">
			<?php
        foreach ($this->posts as $post){
        ?>
			<div class="col-md-3">
				<div class="dashboard">
				<h4><?php echo $post->getUser_id(); ?></h4>
				<p><?php echo $post->getMessage(); ?>
					<span><?php echo date("M d, Y ", strtotime($post->getDate_posted())); ?>
				</span>
				</p>
				
			</div>
			</div>
			<?php  } ?>
		</div>
	</div>
</div>