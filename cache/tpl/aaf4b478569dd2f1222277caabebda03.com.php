	<?php
	
	if(isset($_SESSION['logged_info']))
		header('Location:?c=Main&m=profile');
	
	?>
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-WDM-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a href="#" class="navbar-brand">Z-Cartel</a>
		</div>
		<div class="collapse navbar-collapse navbar-right" id="bs-WDM-navbar-collapse-1">
			<form class="navbar-form navbar-right" role="form" action="index.php" method="POST">
			<div class="form-group">
				<input type="email" name="email" class="form-control" id="email" placeholder="Email">
			</div>
			<div class="form-group">
				<input type="password" name="password" class="form-control" id="password" placeholder="Password">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-default">Sign in</button>
			</div>
			<input type="hidden" name="c" value="Main"/>
			<input type="hidden" name="m" value="loginUser"/>
		</form>
		</div>
	</div>
</nav>
<header>
	<div class="container">
		<div class="col-md-6">
			<h1>Be a ZCartelian</h1>
			<div id="membership">
				<form action="index.php" class="form-horizontal" method="POST">
				<div class="form-group">
					<input type="text" name="name" class="form-control" id="name" placeholder="Name">
				</div>
				<div class="form-group">
					<input type="text" name="username" class="form-control" id="username" placeholder="Username">
				</div>
				<div class="form-group">
					<input type="email" name="email" class="form-control" id="email" placeholder="Email" />
					</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" id="password" placeholder="Password">
				</div>
				<div class="form-group">
					
						<button type="submit" class="btn btn-primary">Register</button>
				
				</div>
				<input type="hidden" name="c" value="Main"/>
				<input type="hidden" name="m" value="create"/>
			</form>
			</div>
		</div>
		<div class="col-md-6">
			<h3>For Nature, Through Art, One Family for Life.</h3>
		</div>
		<div class="clear"></div>
	</div>
</header>
<footer>
<div class="container">
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
				<p class="copyright">Copyright &copy; 2015 - Lyjen Lovendino</p>
		</div>
	</div>		
</div>	
</footer>
