<?php

namespace app\model;

/**
 * Description of postModel
 *
 * @author lyjen
 */
class PostModel extends \rueckgrat\mvc\DefaultDBModel{
    public function __construct() {
        parent::__construct("posts");
    }
    
    public function getByPostId($id){
        $post = new \app\mapper\Post();
        $row = $this->get($id);
        $post->map($row);
        
        return $post;
    }
    public function getUserName(\app\mapper\User $user){
         $stmnt = $this->db->query("SELECT * FROM users WHERE users.id= '".$user->getId()."' ");
        return $stmnt;
    }
    public function getAllPosts(){
       // $stmnt = $this->db->query("SELECT posts.*,users.* FROM posts, users where posts.user_id = users.id ORDER BY posts.id DESC");
        $stmnt = $this->db->query("SELECT * FROM posts ORDER BY posts.id DESC");
        
        $posts = array();
        
        while($row = $stmnt->fetch()){
            $post = new \app\mapper\Post();
            $post->map($row);
            
            $posts[] = $post;
        }
        return $posts;
    }
	
	public function getRandomPosts(){
		$stmnt = $this->db->query("SELECT * FROM posts ORDER BY RAND()");
        
        $posts = array();
        
        while($row = $stmnt->fetch()){
            $post = new \app\mapper\Post();
            $post->map($row);
            
            $posts[] = $post;
        }
        return $posts;
	}
    public function addPost(\app\mapper\Post $post){
		$this->validator->validate(new \app\validator\PostValidator($post));
		$this->create($post);
    }
	
	public function deletePost(\app\mapper\Post $post){
        $this->delete($post);
    }
    
}
