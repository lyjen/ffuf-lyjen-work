<?php

namespace app\model;

/**
 * Description of UserModel
 *
 * @author lyjen
 */
class UserModel extends \rueckgrat\mvc\DefaultDBModel{
    
    protected $validator;
    
    public function __construct() {
        parent::__construct("users");
        
        $this->validator = new \rueckgrat\security\InputValidator;
    }
    
    public function getAllUsers(){
        $stmnt = $this->db->query("SELECT * FROM users");
        
        $users = array();
        
        while($row = $stmnt->fetch()){
            $user = new \app\mapper\User();
            $user->map($row);
            
            $users[] = $user;
        }
        return $users;
    }

    public function registerUser(\app\mapper\User $user){
        $this->validator->validate(new \app\validator\UserValidator($user));
        $this->create($user);
        $_SESSION['registered'] = $user->getEmail();
        $_SESSION['registered'] = TRUE;
    }
    
    public function getById($id){
        $user = new \app\mapper\User();
        $row = $this->get($id);
        $user->map($row);
        
        return $user;
    }
    public function getUserPassword($id){
        $user = $this->getById($id);
        
        return $user->getPassword();
    }
    public function deleteUser(\app\mapper\User $user){
        $this->delete($user);
    }
    
    public function editUser(\app\mapper\User $user){
        $this->validator->validate(new \app\validator\UserValidator($user));
        $this->save($user);
    }
    
    public function checkUser(\app\mapper\User $user){
        $stmnt = $this->db->query("SELECT * FROM users WHERE users.email = '".$user->getEmail()."' AND users.password = '".md5($user->getPassword())."' ");
        return $stmnt;
    }
    public function getUserInfo($user){
        while($row = $user->fetch()){
            return $row;
        }
    }
    
}
