<?php

namespace app\validator;

use rueckgrat\security\ValidationRules;
use rueckgrat\security\ValidationRule;

/**
 * Description of PostValidator
 *
 * @author lyjen
 */
class PostValidator extends \rueckgrat\security\ValidatorContainer{
    
    public function __construct(\app\mapper\Post $post) {
        parent::__construct($post);
		
		$message = new ValidationRule('message', ValidationRules::MIXED );
        $message->setLengths(3,255);
        $message->setErrorMsgGlobal("Speak out");
        
        $this->addRule($message);
        
    }
}
