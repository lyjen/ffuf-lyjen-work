<?php

namespace app\validator;

use rueckgrat\security\ValidationRules;
use rueckgrat\security\ValidationRule;

/**
 * Description of UserValidator
 *
 * @author lyjen
 */
class UserValidator extends \rueckgrat\security\ValidatorContainer{
    
    public function __construct(\app\mapper\User $user) {//user object
        parent::__construct($user);
		
		$name = new ValidationRule('name', ValidationRules::MIXED );
        $name->setLengths(3,50);
        $name->setErrorMsgGlobal("Please enter your name");
        
        $this->addRule($name);
        
        $username = new ValidationRule('username', ValidationRules::MIXED );
        $username->setLengths(3,50);
        $username->setErrorMsgGlobal("Please enter username");
        
        $this->addRule($username);
		
		//$address = new ValidationRule('address', ValidationRules::MIXED );
        //$address->setLengths(3,50);
        //$address->setErrorMsgGlobal("Please enter your address");
        
        //$this->addRule($address);
        
        $password = new ValidationRule('password', ValidationRules::MIXED);
        $password->setLengths(3,50);
        $password->setErrorMsgGlobal("Please enter a password");
        
        $this->addRule($password);
        
        $email = new ValidationRule('email', ValidationRules::EMAIL);
        $email->setLengths(3,50);
        $email->setErrorMsgGlobal("Please enter email");
        
        
    }
}
