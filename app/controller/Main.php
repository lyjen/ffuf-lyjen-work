<?php

namespace app\controller;

use rueckgrat\mvc\DefaultController;
use rueckgrat\security\Input;

/**
 * Description of Main
 *
 * @author lyjen
 */
class Main extends DefaultController {
    
    protected $userModel;
    protected $postModel;
    protected $mainView;
    
    public function __construct(){
        parent::__construct();
        
        $this->userModel = new \app\model\UserModel();
        $this->postModel = new \app\model\PostModel();
        $this->mainView = new \app\view\MainView();
    }
    
    public function index(){
       $users = $this->userModel->getAllUsers();
       return $this->mainView->renderFrontPage($users);
    }
    
    public function create(){
        $user = new \app\mapper\User;
        $user->map(array(
            'name' => Input::p('name'),
            'username' => Input::p('username'),
            'email' => Input::p('email'),
            'password'  => md5(Input::p('password'))
        ));
 
        $this->userModel->registerUser($user);
        
        header('Location:?c=Main&m=login');
    }
    
    public function delete(){
        $user = new \app\mapper\User();
        $user = $this->userModel->getById(Input::g('id'));
        
        $this->userModel->deleteUser($user);
        
        header('Location: ? c=Main&main=index');
    }
    
    public function edit(){
        $user = new \app\mapper\User();
        $user = $this->userModel->getById(Input::g('id'));
        return $this->mainView->renderEditPage($user);
    }
    
    public function editUser(){
        $user = new \app\mapper\User;
        $user->map(array(
            'id' => Input::p('id'),
            'name' => Input::p('name'),
            'address' => Input::p('address'),
            'username' => Input::p('username'),
            'email' => Input::p('email'),
            'password' => $this->userModel->getUserPassword(Input::p('id'))
        ));
        
        $this->userModel->editUser($user);
        header('Location: ?c=Main&m=profile');
    }
    public function login(){
        return $this->mainView->renderLoginPage();
    }
    public function loginUser(){
        $user = new \app\mapper\User;
        $user->map(array(
            'email' => Input::p('email'),
            'password' => Input::p('password')
        ));
        $userInput   = $this->userModel->checkUser($user);
        $checkUser = $userInput->rowCount();
        
        if($checkUser){
           $_SESSION['logged_info'] = $this->userModel->getUserInfo($userInput);
            
            header('Location:?c=Main&m=profile');
        } else {
            $_SESSION['invalid_login'] = TRUE;
            header('Location:?c=Main&m=login');
        }
    }
    public function profile(){
        if(! isset($_SESSION['logged_info'])){
            $this->logout();
        } else {
            $id = $_SESSION['logged_info']['id'];
            $user = $this->userModel->getById($id);
			$posts = $this->postModel->getAllPosts();
            return $this->mainView->renderProfilePage($user,$posts);
        }
    }
    public function logout(){
        if(isset($_SESSION['logged_info'])){
            session_destroy();
        } 

        header('Location:?c=Main&m=index');
    }
}
