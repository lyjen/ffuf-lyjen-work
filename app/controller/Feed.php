<?php

namespace app\controller;

use rueckgrat\mvc\DefaultController;
use rueckgrat\security\Input;

/**
 * Description of Feed
 *
 * @author lyjen
 */
class Feed extends DefaultController{
    
    protected $postModel;
	protected $mainView;
    
    public function __construct(){
        parent::__construct();
        
        $this->postModel = new \app\model\PostModel();
        $this->mainView = new \app\view\MainView();
    }
    
    public function index(){

        $posts = $this->postModel->getRandomPosts();
        return $this->mainView->renderHomePage($posts);
    }
	
	public function addPost(){
        $post = new \app\mapper\Post;
        $post->map(array(
            'message' => Input::p('message'),
            'user_id'  => Input::p('user_id')
        ));
        $this->postModel->addPost($post);
        
        header('Location:?c=Main&m=profile');
    }
	
	public function deletePost(){
		$post = new \app\mapper\Post();
        $post = $this->postModel->getByPostId(Input::g('id'));
        
        $this->postModel->deletePost($post);
        
        header('Location: ? c=Main&main=profile');
	}
}
