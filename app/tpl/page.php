<!DOCTYPE html>
<html>
    <head>
        <title><?php echo isset($title) ? $title : 'MVC-FrameWork'; ?></title>
		 <meta  charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <?php
        if (isset($cssFiles)) {
            foreach ($cssFiles AS $file) {
                echo '<link rel="stylesheet" type="text/css" href="' . $file . '">' . "\n";
            }
        }
        ?>
    </head>
    <body>
        <div id="content">
            <?php echo $pageContent; ?>
        </div>
        <script type="text/javascript" src="public/js/main.js"></script>
        <?php
        if (isset($jsFiles)) {
            foreach ($jsFiles AS $file) {
                echo '<script type="text/javascript" src="' . $file . '"></script>' . "\n";
            }
        }
        if (isset($callbacks)) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . $callbacks . ');});</script>' . "\n";
        }
        ?>
    </body>
</html>
