<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-WDM-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a href="#" class="navbar-brand">Z-CARTEL</a>
		</div>
		<div class="collapse navbar-collapse navbar-right" id="bs-WDM-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="?c=Main&m=profile"><?php echo $this->logged_user->getUsername(); ?></a></li>
				<li><a href="?c=Feed&m=index&user_id=<?php echo $this->logged_user->getId();?>">Dashboard</a></li>
				<li><a href="?c=Main&m=logout">Signout</a></li>
          </ul>
		</div>
	</div>
</nav>
<div class="wrapper">
	<div class="container">
		<div class="col-md-4">
			<div id="profile_id">
				<h3><?php echo $this->logged_user->getId(); ?></h3>
			</div>
			<h2>About Me</h2>
			<a href="?c=Main&m=edit&id=<?php echo $this->logged_user->getId(); ?>" id="edit">Edit</a>
			<div id="about_me">
				<div class="about">
					<div class="about_label"><h4>Name</h4></div>
					<div class="about_user"><?php echo $this->logged_user->getName(); ?></div>
					<div class="clear"></div>
				</div>
				<div class="about">
					<div class="about_label"><h4>Username</h4></div>
					<div class="about_user"><?php echo $this->logged_user->getUsername(); ?></div>
					<div class="clear"></div>
				</div>
				<div class="about">
					<div class="about_label"><h4>Address</h4></div>
					<div class="about_user"><?php echo $this->logged_user->getAddress(); ?></div>
					<div class="clear"></div>
				</div>
				<div class="about">
					<div class="about_label"><h4>Email</h4></div>
					<div class="about_user"><?php echo $this->logged_user->getEmail(); ?></div>
					<div class="clear"></div>
				</div>
				<div class="about">
					<div class="about_label"><h4>Date Joined</h4></div>
					<div class="about_user"><?php echo date("M d, Y ", strtotime($this->logged_user->getDate_registered())); ?></div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="row">
				<h3>Echo Your Voice!</h3>
				<form name="post" id="post" method="post" action="index.php">
				<div class="form-group">
					<textarea name="message" id="message" class="form-control" rows="3"></textarea>
				</div>
				<input type="hidden" name="c" value="Feed"/>
				<input type="hidden" name="m" value="addPost"/>
				<input type="hidden" name="user_id" value="<?php echo $this->logged_user->getId();?>"/>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Share</button>
				</div>
			</form>	
			</div>
			<div class="row" id="posts">
			<?php
				foreach ($this->posts as $post){
				$logged = $this->logged_user->getId();
				$post_id = $post->getUser_id();
			?>
				<div class="post">
				<?php if($logged == $post_id){ ?>
					<a href="?c=Feed&m=deletePost&id=<?php echo $post->getId(); ?>">delete</a>
				<?php } ?>
					<h4><?php echo $post->getUser_id(); ?></h4>
					<p><?php echo $post->getMessage(); ?>
						<span><?php echo date("M d, Y ", strtotime($post->getDate_posted())); ?>
					</span>
					</p>
				<?php  } ?>
				</div>
			</div>
		</div>
	</div>
</div>


