<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
		 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-WDM-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
		</button>
		<a href="#" class="navbar-brand">Z-Cartel</a>
		</div>
		<div class="collapse navbar-collapse navbar-right" id="bs-WDM-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="?c=Main&m=profile"><?php echo $this->user->getName(); ?></a></li>
				<li><a href="?c=Feed&m=index">Dashboard</a></li>
				<li><a href="?c=Main&m=logout">Signout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav> 
<div class="wrapper">
	<div class="container">
		<h3>Update Your Profile</h3>
		<form action="index.php" class="form-horizontal" method="POST">
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" id="name" value="<?php echo $this->user->getName(); ?>" />
				</div>
			</div>
			<div class="form-group">
				<label for="address" class="col-sm-2 control-label">Address</label>
				<div class="col-sm-10">
					<input type="text" name="address" class="form-control" id="address" value="<?php echo $this->user->getAddress(); ?>"/>
				</div>
			</div>
			<div class="form-group">
				<label for="username" class="col-sm-2 control-label">Username</label>
				<div class="col-sm-10">
					<input type="text" name="username" class="form-control" id="username" value="<?php echo $this->user->getUsername(); ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input type="email" name="email" class="form-control" id="email" value="<?php echo $this->user->getEmail(); ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control" id="password" value="<?php echo $this->user->getPassword(); ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Update</button>
				</div>
			</div>
			<input type="hidden" name="id" value="<?php echo $this->user->getId(); ?>"/>
			<input type="hidden" name="date_registered" value="<?php echo $this->user->getDate_registered(); ?>"/>
			<input type="hidden" name="c" value="Main"/>
			<input type="hidden" name="m" value="editUser"/>
		</form>
	</div>
</div>