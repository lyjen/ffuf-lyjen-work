<?php

namespace app\dbo;

/**
 * Description of User
 *
 * @author lyjen
 */
class User extends \rueckgrat\db\Mapper {
        
        protected $name;
		protected $username;
		protected $address;
		protected $email;
        protected $password;
        protected $date_registered;


    public function __construct(){
    	parent::__construct();
    }
    
    function getName() {
        return $this->name;
    }

    function getUsername() {
        return $this->username;
    }
	
	function getAddress() {
        return $this->address;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }
    
    function getDate_registered() {
        return $this->date_registered;
    }
}
