<?php

namespace app\dbo;

/**
 * Description of Post
 *
 * @author lyjen
 */
class Post extends \rueckgrat\db\Mapper{
    
    protected $message;
    protected $user_id;
    protected $date_posted;
	
    public function __construct() {
        parent::__construct();
    }

	
    function getMessage() {
        return $this->message;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function getDate_posted() {
        return $this->date_posted;
    }
}
