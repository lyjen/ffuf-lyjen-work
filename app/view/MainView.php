<?php

namespace app\view;

/**
 * Description of MainView
 *
 * @author lyjen
 */
class MainView extends \rueckgrat\mvc\FastView{
    
    protected $users;
    protected $user;
    protected $logged_user;
    protected $posts;


    public function __construct() {
        parent::__construct();
        $this->cacheDisabled = TRUE;
        $this->title = 'Z-Cartel';
        $this->cssFiles[]   = 'public/css/bootstrap.css';
        $this->jsFiles[] = 'public/js/jqueryv1.11.2.js';
		$this->jsFiles[] = 'public/js/bootstrap.min.js';
    }
    
    public function renderFrontPage($users){
        $this->users = $users;
        $this->pageContent = $this->getCompiledTpl("main/main");
        return $this->renderMainPage();
    }
    
    public function renderEditPage(\app\mapper\User $user){
        $this->user = $user;
		$this->title = 'Edit';
        $this->pageContent = $this->getCompiledTpl("main/edit");
        return $this->renderMainPage();
    }
    
    public function renderLoginPage(){
		$this->title = 'Login';
        $this->pageContent = $this->getCompiledTpl("main/login");
        return $this->renderMainPage();
    }
    
    public function renderProfilePage(\app\mapper\User $logged_user, $posts){
        $this->logged_user = $logged_user;
        $this->posts = $posts;
		$this->title = 'Profile';
        $this->pageContent = $this->getCompiledTpl("main/profile");
        return $this->renderMainPage();
    }
    
    public function renderHomePage($posts){
        $this->posts = $posts;
        $this->pageContent = $this->getCompiledTpl("main/home");
        return $this->renderMainPage();
    }
}